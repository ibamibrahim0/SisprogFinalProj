import requests
import json
from tqdm import tqdm
import time

def get_text(filename, overlay=False, api_key='wadidaw', language='eng'):
   payload = {'isOverlayRequired': overlay, 'apikey': api_key, 'language': language}
   with open(filename, 'rb') as f:
        r = requests.post('https://api.ocr.space/parse/image',
                          files={filename: f},
                          data=payload,
                          )
        return r.content.decode()

def get_voice(text="hello world"):
	url = "https://api.voicerss.org/?key=fb14e3a5eb884ef9916c4d42bbb7b705&hl=en-us&src={}".format(text)
	response = requests.get(url, stream=True)

	with open("result.mp3", "wb") as handle:
	    for data in tqdm(response.iter_content()):
	        handle.write(data)

def get_server_data():
  req = requests.get('https://api.myjson.com/bins/rr49j')
  req_status = req.content.decode()
  status = json.loads(req_status) 
  parsed_status = status.get("status")
  return status

def download_image(url):
  img_response = requests.get(url, stream=True)

  with open("sample.jpg", "wb") as handle:
      for data in tqdm(img_response.iter_content()):
          handle.write(data)

def change_server_status():
  param = {"status": False, "image": ""}
  data = json.dumps(param) 
  headers = {'Content-type': 'application/json'}
  req_change = requests.put('https://api.myjson.com/bins/rr49j', data=data, headers=headers)
  print(">> change server status complete")

def process_image():
  response = get_text(filename='sample.jpg', api_key='b139c8589c88957')

  data = json.loads(response, strict=False)
  parsed_results = data.get("ParsedResults")[0].get("ParsedText")
  print(parsed_results)

  print("Downloading voice...")
  get_voice(parsed_results)

# main method
while(True):
  status = get_server_data().get("status")
  image_url = get_server_data().get("image")
  if(status == True):
    print("status is true, downloading image")
    download_image(image_url)
    print("download complete")
    change_server_status()
    process_image()
  else:
    print("status is false, re-looping")
    time.sleep(2)
